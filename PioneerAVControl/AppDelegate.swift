//
//  AppDelegate.swift
//  PioneerAVControl
//
//  Created by Lothar Heinrich on 22.03.20.
//  Copyright © 2020 Lothar Heinrich. All rights reserved.
//

import Cocoa
import SwiftUI

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

//    var window: NSWindow!
    var statusItem: NSStatusItem?

    var popover: NSPopover?
    var contentView: ContentView?

    var detector: AnyObject?

    let pioneerConnection = PioneerConnection.sharedInstance

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        initStatusItem()
        initPopover()

//        DispatchQueue.global(qos: .userInitiated).async {
            self.pioneerConnection.connect()
//        }
        // Create the SwiftUI view that provides the window contents.
//        let contentView = ContentView()

        
//        return;
//        // Create the window and set the content view.
//        window = NSWindow(
//            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
//            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
//            backing: .buffered, defer: false)
//        window.center()
//        window.setFrameAutosaveName("Main Window")
//        window.contentView = NSHostingView(rootView: contentView)
//        window.makeKeyAndOrderFront(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
//        print("applicationWillTerminate")
        let pioneerConnection = PioneerConnection.sharedInstance
        pioneerConnection.disconnect()
    }

    fileprivate func initStatusItem() {
        statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
        let itemImage = NSImage(named: "AmpIcon")
        itemImage?.isTemplate = true
        statusItem?.button?.image = itemImage
        
        statusItem?.button?.target = self
        statusItem?.button?.action = #selector(showPopover)
        
        
    }
    fileprivate func initPopover() {
       popover = NSPopover()
       popover?.behavior = .transient
    }
    
    @objc fileprivate func showPopover() {
        
        guard let popover = popover, let button = statusItem?.button else { return }
        if (popover.isShown) {
            popover.close()
            return
        }
        
        if contentView == nil {
            contentView = ContentView()
            detector = NSEvent.addGlobalMonitorForEvents(matching:[NSEvent.EventTypeMask.leftMouseDown, NSEvent.EventTypeMask.rightMouseDown], handler: { [weak self] event in
                self?.hidePopover()
            }) as AnyObject

        }
    
        popover.contentViewController = NSHostingController(rootView: contentView.environmentObject(PioneerModel(pioneerConnection: pioneerConnection)));
        
        popover.show(relativeTo: button.bounds, of: button, preferredEdge: .minY)
        
    }
    
    fileprivate func hidePopover() {
            popover?.performClose(nil)
            if let temp: AnyObject = detector {
                NSEvent.removeMonitor(temp)
            }
        }
}

