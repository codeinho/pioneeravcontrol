//
//  PioneerControlService.swift
//  PioneerControl
//
//  Created by Lothar Heinrich on 12.06.15.
//  Copyright (c) 2015 Lothar Heinrich. All rights reserved.
//

import Foundation

public enum PioneerError: Error {
    case notConnected (errText: String)
}

public protocol PioneerConnectionProtocol {
    func connect() throws
    func disconnect()
    func sendCode(code: String) throws
    func requestUpdateAll() throws
    var isConnected: Bool { get }
}

extension Notification.Name {
    static let pioneerConnectionDidReceiveData = Notification.Name("pioneerConnectionDidReceiveData")
    static let pioneerConnectionDidReceiveUpdate = Notification.Name("pioneerConnectionDidReceiveUpdate")
    static let pioneerConnectionStateDidChange = Notification.Name("pioneerConnectionStateDidChange")
}


// Connection to Pioneer AV Receiver as singleton
public class PioneerConnection : NSObject, GCDAsyncSocketDelegate, PioneerConnectionProtocol {
    
    public static let sharedInstance = PioneerConnection();
    
    public struct KeyValue {
        var Key : String // codePrefixes
        var Value : String
    }
    
    
    fileprivate let codePrefixes = [ // für requests: code : length
        "VOL" : 3,
        "FN" : 2,
        //"MO" : 0,
        //"MF" : 0,
        "SR" : 3,
        "MUT" : 1,
        "PWR" : 1,  //PWR1=off
        "PR" : 3,  // Tuner preset
    ]
    
    // https://github.com/robbiehanson/CocoaAsyncSocket
    fileprivate let mySocket = GCDAsyncSocket()// GCDAsyncSocket!
    fileprivate let codeSuffix = "\r\n"
    
    private override init() {
        super.init()
        mySocket.setDelegate(self, delegateQueue: DispatchQueue.global(qos: .userInitiated))
    }
    
    
    public var isConnected: Bool {
        get {
            return mySocket.isConnected
        }
    }
    
    public func requestUpdateAll() {
        print("reqAll", isConnected)
        if !isConnected {return}
        
        let updateCodes = ["?V", "?M", "?F", "?S", "?PR"];
        
        var i = 0
        for code in updateCodes {
            
            let waitSeconds = 0.1; // 10 hundertstel
            
            DispatchQueue.main.asyncAfter(deadline: .now() + waitSeconds * Double(i)) {
                do {
                    try self.sendCode(code: code)
                } catch {
                    print("error: sendCode " + code) // $lot
                }
            }
            i = i + 1
        }
    }
    
    
    public func connect() {
        
        // connect
        let host = "192.168.188.29" //vsx922k" //"192.168.178.22" //self.host.stringValue // "192.168.178.22"
        let port: UInt16 = 23 //Int(self.port.intValue) // 23
        //        print("try connect \(host):\(port) ...")
        
        var error: NSError?
        
        let success: Bool
        do {
            try mySocket.connect(toHost: host , onPort: port, withTimeout: 2.0)
            success = true // does *NOT* necessarly mean that connection is established (e.g. host could be off); we have to wait for the notification
        } catch let error1 as NSError {
            error = error1
            success = false
        }
        
        if !success {
            if let error = error {
                print("connection failed: \(error.localizedDescription)")
            } else {
                print("connection failed (unknown error)")
            }
        } else {
//                        print("connection success")
        }
        
        
        
    }
    
    
    public func disconnect() {
        mySocket.disconnect()
    }
    
    fileprivate func onDisconnected() {
        //        print("disconnected or connection failed")
        NotificationCenter.default.post(name: .pioneerConnectionStateDidChange, object: self)
    }
    
    fileprivate func onConnected() {
                print("connected")
        
        NotificationCenter.default.post(name: .pioneerConnectionStateDidChange, object: self)
        // connected --> begin read. next read is in didRead..
        mySocket.readData(withTimeout: -1.00, tag: 0)
        
    }
    
    public func sendCode(code: String) throws {
        if (!self.isConnected) {
            throw PioneerError.notConnected(errText: "cannot send, not connected")
        }
        let data = ((code + codeSuffix)).data(using: String.Encoding.utf8)
        mySocket.write(data, withTimeout: 0.5, tag: 0)
        
    }
    
    public func socket(_ socket : GCDAsyncSocket, didConnectToHost host:String, port p:UInt16) {
        onConnected()
    }
    
    public func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        //        print("didWriteData")
    }
    
    public func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        let data_str = String(data: data, encoding: .utf8)!
        print("\(Date()) \(data_str)")
        NotificationCenter.default.post(name: .pioneerConnectionDidReceiveData, object: self, userInfo:["data" : data_str])
        if let parseResult = self.parseIncomingData(data: data_str) {
            
            NotificationCenter.default.post(name: .pioneerConnectionDidReceiveUpdate,
                                            object: self,
                                            userInfo:[parseResult.Key : parseResult.Value])
            // handle PWR1 --> disconnect
            if parseResult.Key == "PWR" && parseResult.Value == "1" {
                self.disconnect()
            } else if (parseResult.Key == "VOL") {
                //adjustVolumeHack(parseResult.Value)
            }

            
        }
        mySocket.readData(withTimeout: -1.00, tag: 0)
        
    }
    
    public func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        if let err = err { print("error: \(err)") }
        // vsx off when trying to connect ==>
        // error: Optional(Error Domain=GCDAsyncSocketErrorDomain Code=3 "Attempt to connect to host timed out" UserInfo={NSLocalizedDescription=Attempt to connect to host timed out})
        onDisconnected()
    }
    /*
     see
     http://www.pioneerelectronics.com/pio/pe/images/portal/cit_3424/492741580VSX92TXH_RS232C_Protocols.pdf
     
     ---SET: number is prefix, GET: number is suffix---
     02FN=Tuner
     04FN=DVD
     15FN=Apple TV
     26FN=Airplay
     
     
     --Volume--
     090VL = Volume=70
     MO=mute on
     MF=mute off
     
     --- Listening mode ---
     ?S=Abfrage Listening Mode
     SR0112 = EXTENDED STEREO   (7CH-STEREO)
     set listenig mode
     0112SR = EXTENDED STEREO   (7CH-STEREO)
     0007SR = NORMAL DIRECT
     
     
     --is on / off ----
     ?V --> PWR0/1 ("power")
     PO/PF --> an/aus-stellen
     
     
     Beispiel sonstiger Meldungen:
     GEH01020"Seit Der Himmel"
     GEH02023"0:23"
     GEH03021"Element Of Crime"
     GEH04022"Romantik"
     GEH05024"Indie Rock"
     GEH06026"AirPlay"
     GEH07027"1411 kbps"
     GEH08031"100"
     */
    fileprivate func parseIncomingData(data : String) -> KeyValue? {
        //      print(data)
        let filteredCodes = Array(codePrefixes.keys).filter({ code in
            data.hasPrefix(code)
        })
        
        if filteredCodes.count == 0 {
            //print("data=\(data) not parsed")
            return nil
        }
        if filteredCodes.count > 1 {
            print("data=\(data) ambiguous, matches \(filteredCodes)")
            return nil
        }
        
        // exactly 1 item in filteredCodes
        
        let key = filteredCodes.first!
        let codeLen = key.count
        
        let valueLen = codePrefixes[key]!;
        
        let range = data.index(data.startIndex, offsetBy: codeLen)..<data.index(data.startIndex, offsetBy: codeLen+valueLen)
        let valueStr = data[range]
        
        return KeyValue(Key: key, Value: String(valueStr))

    }
    
    
    // HACK for problem where VSX automatically decreses volume (3dB), see https://www.youtube.com/watch?v=YTdIvQwTeuA
    // note that this cannot be done in PioneerModel, since it is not save that there is a PioneerModel when popup is not open
    private var previousVolume = -1 // -1 => not set
    private func adjustVolumeHack(_ newVolume_str: String) {
        
        func volumeToDezibel(_ volume:Double) -> Double {
            return volume / 2 - 80.5
        }

        if let newVolume = Int(newVolume_str) {
            let prevVol_dB = volumeToDezibel(Double(previousVolume))
            let newVol_dB = volumeToDezibel(Double(newVolume))
            
            if (prevVol_dB == newVol_dB || previousVolume == -1) { // most probably response to our request
                previousVolume = newVolume
                return
            }
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            print("\(formatter.string(from: Date())) old=\(previousVolume) new=\(newVolume) diff=\(prevVol_dB-newVol_dB)")
            //
            if (prevVol_dB - newVol_dB == 3.0 || prevVol_dB - newVol_dB == 2.5) { // vsx sends either 3dB or pairs 0.5+2.0
                let targetVol = min(previousVolume + 1,180)
                let volCode = "\(String(format:"%03d", targetVol))VL"
                print("sending hack: Volume+3dB back to \(String(format: "%.1f",volumeToDezibel(Double(targetVol)))) --> \(volCode)")
                
                do {
                    try self.sendCode(code: volCode)
                } catch {
                    print("Unexpected error: \(error).")
                }
            }
            previousVolume = newVolume
        }
        
    }
    
    deinit {
        mySocket.delegate = nil
        mySocket.delegateQueue = nil
    }
    
}




