//
//  FunctionModes.swift
//  PioneerAVControl
//
//  Created by Lothar Heinrich on 27.03.20.
//  Copyright © 2020 Lothar Heinrich. All rights reserved.
//

import Foundation


/*
 02FN=Tuner
 04FN=DVD
 15FN=Apple TV (HDMI)
 26FN=Airplay
 */

struct FunctionMode { // Tuner etc.
    let code: Int  // code in Pioneer,2=Tuner, 26=HMG
    let name: String
    var hidden = false
    
    // returns
    // nil for code=0
    // 01FN for code=1
    // 12FN for code=12
    var codePioneer: String? {
        get {
            if (code == 0) {
                return nil
            }

            return "\(String(format:"%02d", code))FN"
        }
    }
}


struct FunctionModes {
    
    static let availableFunctionModes = [
        FunctionMode(code: 0, name: "Unknown", hidden: true), // DON'T REMOVE!!!
        FunctionMode(code: 2, name: "Tuner"),
        FunctionMode(code: 4, name: "DVD", hidden: true),
        FunctionMode(code: 15, name: "AppleTV"),
        FunctionMode(code: 26, name: "Airplay"), // = H.M.G.
    ]
    static var visibleFunctionModes: [FunctionMode] {
        get {
            return availableFunctionModes.filter {$0.hidden == false}
        }
    }
    static func pickerIndex(code: Int) -> Int {
        if let index = visibleFunctionModes.firstIndex(where: { $0.code == code }) {
            return index
        } else {
            return -1
        }
    }
    static func functionCodeFromPickerIndex(pickerIndex: Int) -> FunctionMode {
        if pickerIndex < 0 || pickerIndex > availableFunctionModes.count - 1 {
            return availableFunctionModes[0]
        }
        return self.visibleFunctionModes[pickerIndex]
    }
    static func index(code: Int) -> Int {
        if let index = availableFunctionModes.firstIndex(where: { $0.code == code }) {
            return index
        } else {
            return 0
        }
    }
}
