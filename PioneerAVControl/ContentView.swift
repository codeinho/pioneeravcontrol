//
//  ContentView.swift
//  PioneerAVControl
//
//  Created by Lothar Heinrich on 22.03.20.
//  Copyright © 2020 Lothar Heinrich. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var pioneerModel: PioneerModel
    
    @State private var isDragging = false
    @State private var currentPosition: CGSize = .zero
    @State private var newPosition: CGSize = .zero
    @State private var oldPosition: CGSize = .zero
    @State private var lastHuerde = 0
    
    var drag: some Gesture {
        DragGesture()
            .onChanged { value in
                
                self.isDragging = true
//                print(CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height))
//                print(value.translation.width + self.newPosition.width)
                let delta = value.translation.width + self.newPosition.width
                if (Int(delta) > self.lastHuerde + 10) {
                    self.lastHuerde = self.lastHuerde + 10
                    print("\(self.lastHuerde)")
                }
                self.currentPosition = CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height)
                
        }
        .onEnded { value in self.isDragging = false
            print("end")
//            print(CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height))
            self.currentPosition = CGSize(width: value.translation.width + self.newPosition.width, height: value.translation.height + self.newPosition.height)
//            self.newPosition = self.currentPosition
            self.newPosition = .zero
            self.currentPosition = .zero
            self.lastHuerde = 0
        }
    }
    
    var body: some View {
        let binding = Binding<Int>(
            get: { self.pioneerModel.functionModeCode_PickerIndex },
        set: { (newIndex) in
            
            self.pioneerModel.functionModeCode_PickerIndex = newIndex
            self.pioneerModel.setFunctionMode(newIndex: newIndex)
        })
        
        var volumeColor = Color.green
        if self.pioneerModel.volume > 140 {
            volumeColor = Color.red
        } else if self.pioneerModel.volume > 110 {
            volumeColor = Color.yellow
        }
        
        return VStack {
            
            VStack {
                HStack {
                    Button(action: { self.pioneerModel.increse(volume: -5) }) { Text("❮❮") }
                    Button(action: { self.pioneerModel.increse(volume: -1) }) { Text("❮") }
                    
                    Text("\(String(format: "%.1f", PioneerModel.volumeToDezibel(volume:pioneerModel.volume))) dB")
                        .bold()
                        .foregroundColor(volumeColor)
                        .offset(x: self.currentPosition.width, y: self.currentPosition.height)
                        .frame(minWidth: 60, maxWidth: 60)
                        .gesture(drag)
                    
                    Button(action: { self.pioneerModel.increse(volume: 1) }) { Text("❯") }
                    Button(action: { self.pioneerModel.increse(volume: 5) }) { Text("❯❯") }
                   
                   
                    Toggle(isOn: Binding (
                        get: {
                            self.pioneerModel.mute
                    },
                        set: {(newValue) in
                            self.pioneerModel.muteToggle()
                    })
                    ) {
                        Text("mute")
                    }
                }
                
                Picker("Device", selection: binding) { //$pioneerModel.functionModeCode_PickerIndex) {
                    
                                   ForEach(0..<FunctionModes.visibleFunctionModes.count) { index in
                                    Text(FunctionModes.visibleFunctionModes[index].name)
                                   }
                               }
                                .labelsHidden()
                                .pickerStyle(SegmentedPickerStyle())
                               
//                if (pioneerModel.functionModeCode_Index == 0) {
//                    Text("Source \(FunctionModes.availableFunctionModes[pioneerModel.functionModeCode_Index].name)").foregroundColor(Color.red)
//                }
                HStack {
                    Button(action: { self.pioneerModel.tunerPresetDecrement() }) { Text("❮") }
                    Text("Tuner \(self.pioneerModel.tunerPreset)")
                    Button(action: { self.pioneerModel.tunerPresetIncrement() }) { Text("❯") }
                }.disabled(self.pioneerModel.functionModeCode != 2) // enable when tuner is on
                
            }.disabled(!self.pioneerModel.connected) // disable when not connectet
            
            Divider()
           
            HStack {
                Button(action: {
                               self.pioneerModel.connectToggle()
                           }) {
                               if (self.pioneerModel.connected) {
                                   Text("Disconnect").foregroundColor(Color.green)
                               } else {
                                   Text("Connect").foregroundColor(Color.red)
                               }
                }
                
                Spacer()
                
//                Text(".")
//
//                Button(action: {
//                    NSApplication.shared.terminate(_:nil)
//                }) {
//                    Text("Quit")
//                }
               MenuButton("") {
                   Button(action: {
                        NSApplication.shared.terminate(_:nil)
                   }) {
                       Text("Quit")
                   }
               }.menuButtonStyle(BorderlessPullDownMenuButtonStyle()).frame(width:30)
            }
            
        }.padding(5).frame(width: 300).onAppear {
//            print("ContentView appeared!")
        }.onDisappear {
//            print("ContentView disappeared!") // never called?!
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(PioneerModel())
    }
}
