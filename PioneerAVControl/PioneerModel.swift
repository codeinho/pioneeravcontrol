//
//  PioneerModel.swift
//  PioneerAVControl
//
//  Created by Lothar Heinrich on 26.03.20.
//  Copyright © 2020 Lothar Heinrich. All rights reserved.
//

//import Combine

struct PioneerConnectionTestDummy: PioneerConnectionProtocol {
    
    var isConnected: Bool = false
    
    func disconnect() {
        
    }
    
    func sendCode(code: String) {
        return
    }
    
    func requestUpdateAll() {
        
    }
    
    func connect() {
        print("Testdummy: connect()")
    }
    
}


final class PioneerModel: ObservableObject {
    var pioneerConnection: PioneerConnectionProtocol
    
    @Published var connected: Bool = false
    @Published var volume: Double = 0 // 0-185 bzw. -81dB - 12dB
    @Published var mute: Bool = false
    @Published var functionModeCode: Int = 0
    @Published var functionModeCode_PickerIndex: Int = -1
    @Published var functionModeCode_Index: Int = 0
    @Published var tunerPreset = ""
    
    init() {
        self.pioneerConnection = PioneerConnectionTestDummy()
        startObservePioneerConnection()
    }
    
    init(pioneerConnection: PioneerConnectionProtocol) {
        print("init(...)")
        self.pioneerConnection = pioneerConnection
        
        
        // FIXME: hier habe ich mit der Reihenfolge (und in den Methoden) experimentiert $lot
        // das sollte evt. wieder rückgängig gemacht werden
        startObservePioneerConnection()
        updateConnectionStateAndRequestAll()
    }
    
    
    func startObservePioneerConnection() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(onPioneerConnectionDidReceiveUpdate(notification:)), name: .pioneerConnectionDidReceiveUpdate, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onPioneerConnectionStateDidChange(notification:)), name: .pioneerConnectionStateDidChange, object: nil)
    }
    
    func stopObservePioneerConnection() {
        NotificationCenter.default.removeObserver(self, name: .pioneerConnectionDidReceiveUpdate, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: .pioneerConnectionStateDidChange, object: nil)
    }
    
    static func volumeToDezibel(volume:Double) -> Double {
        return volume / 2 - 80.5
    }
    
    func increse(volume: Int) { // TODO not nice
        if (volume == 0) {
            return;
        }
        
        if (volume == -1) {
            self.sendCode("VD")
        } else if (volume == 1 ) {
            self.sendCode("VU")
        } else {
            let maxVol = 185.0
            let minVol = 0.0
            var newVol = self.volume + Double(volume)
            newVol = Double.minimum(maxVol, newVol)
            newVol = Double.maximum(minVol, newVol)

            self.sendCode("\(String(format:"%03d", Int(newVol)))VL")
        }
        
        
    }
    func setFunctionMode(newIndex: Int) {
     
        let functionMode = FunctionModes.functionCodeFromPickerIndex(pickerIndex: newIndex)
        
        if let codePioneer = functionMode.codePioneer {
//            print(codePioneer)
            self.sendCode(codePioneer)
        } else {
            // nicht senden, nur picker zurücksetzen:
            functionModeCode_PickerIndex = -1
        }

                
    }
    
    func tunerPresetIncrement() {
        self.sendCode("TPI")
    }
    
    func tunerPresetDecrement() {
        self.sendCode("TPD")
    }
    
    func muteToggle() {
        self.sendCode("MZ")
    }
    
    func connectToggle() {
        if (connected) {
            pioneerConnection.disconnect()
        } else {
            try? pioneerConnection.connect()
        }
    }
    
    fileprivate func sendCode(_ code: String) {
        do {
            try pioneerConnection.sendCode(code: code)
        } catch PioneerError.notConnected(let errText) {
            print("error: \(errText)")
        } catch {
            print("Unexpected error: \(error).")
        }
    }
    
    fileprivate func updateConnectionStateAndRequestAll() {
        DispatchQueue.main.async {
            if self.pioneerConnection.isConnected {
                self.connected = true

                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    try? self.pioneerConnection.requestUpdateAll()
                }
            } else {
                self.connected = false
            }
        }
    }
    
    fileprivate func updateFunctionMode(_ functionModeCode : Int) {
        print("updateFunctionMode \(functionModeCode)")
        /*
         02FN=Tuner
         04FN=DVD
         15FN=Apple TV
         26FN=Airplay
         
         */
        DispatchQueue.main.async {
            self.functionModeCode = functionModeCode
            self.functionModeCode_PickerIndex = FunctionModes.pickerIndex(code: functionModeCode)
            self.functionModeCode_Index = FunctionModes.index(code: functionModeCode)
        }
    }
    
    @objc fileprivate func onPioneerConnectionStateDidChange(notification: NSNotification) {
        updateConnectionStateAndRequestAll()
    }
    
    @objc fileprivate func onPioneerConnectionDidReceiveUpdate(notification: NSNotification) {
        
        DispatchQueue.main.async {
            if let newVolume_str = notification.userInfo?["VOL"] as? String {
                if let newVolume = Int(newVolume_str) {
                    let vol = Double(newVolume)
                    self.volume = vol
                } else {
                    print("error: expected Int for volume, but is=\(newVolume_str)")
                }
            }
            if let newMute = notification.userInfo?["MUT"] as? String {
                self.mute = (newMute=="0")
            }
            if let newFunctionMode = notification.userInfo?["FN"] as? String {
                self.updateFunctionMode(Int(newFunctionMode) ?? 0)
            }
            if let newTunerPreset = notification.userInfo?["PR"] as? String {
//                print("tuner preset \(newTunerPreset)")
                self.tunerPreset = newTunerPreset
            }
        }
    }
    
    deinit {
        stopObservePioneerConnection()
    }
}
